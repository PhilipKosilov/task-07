package com.epam.rd.java.basic.task7.db.entity;

public class User {

	private int id;

	private String login;

	private User(String login){
		this.login = login;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		return new User(login);
	}

	@Override
	public String toString() { return login; }

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof User)) return false;
		User other = (User)obj;
		return login.equals(other.getLogin());
	}

}
