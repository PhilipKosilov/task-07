package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;

	private String name;

	private Team(String name) { this.name = name; }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		return new Team(name);
	}

	@Override
	public String toString() { return name; }

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Team)) return false;
		Team other = (Team)obj;
		return name.equals(other.getName());
	}

}
