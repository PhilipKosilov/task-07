package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private static Connection cn;
	private static final Logger LOG = Logger.getLogger(DBManager.class.getName());

	/**
	 * Returns an instance of this Database manager with opened connection.
	 * @return an instance of DBManager that's using MySQL JDBC driver
	 * 		   and is connected to a database provided by URL in app.properties file.
	 */
	public static synchronized DBManager getInstance() {
		if (instance == null) instance = createInstance();
		return instance;
	}

	private DBManager() throws ClassNotFoundException, SQLException {
		loadJDBCDriver("com.mysql.cj.jdbc.Driver");// NOT org.gjt.mm.mysql.Driver
		String URL = getURLProperty("app.properties");
		openConnection(URL);
	}

	private static DBManager createInstance() {
		try {
			return new DBManager();
		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	private void loadJDBCDriver(String URL) throws ClassNotFoundException {
		Class.forName(URL);
	}

	// For now there is a single Connection that is opened and never closed.
	// If you open and close Connection in every method, testDemo will produce an ERROR 40XL1 on tearDown dropping users table (line 95)
	private void openConnection(String URL) throws SQLException {
		LOG.info("Acquiring connection...");
		cn = DriverManager.getConnection(URL);
		LOG.info("Connection established.");
	}

	private String getURLProperty(String path) {
		try (InputStream input = new FileInputStream(path)) {
			Properties prop = new Properties();
			prop.load(input);

			return prop.getProperty("connection.url");
		} catch (IOException ie) {
			ie.printStackTrace();
		}
		return null;
	}

	/**
	 * Returns a list of users in a given Database.
	 * @return a list of all users in a given Database.
	 * @throws DBException
	 */
	public List<User> findAllUsers() throws DBException {
		try {
			Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM users ORDER BY id");

			List<User> list = new ArrayList<>();
			while(rs.next()){
				int userID = rs.getInt(1);
				String userLogin = rs.getString(2);
				User user = User.createUser(userLogin);
				user.setId(userID);
				list.add(user);
			}
			LOG.info("Users found: " + list);

			return list;
		} catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
		//return null;
	}

	/**
	 * Inserts a new user to a given Database.
	 * @param user
	 * @return some boolean that wasn't specified in the requirements.
	 * @throws DBException
	 */
	public boolean insertUser(User user) throws DBException {
		try {
			// Adding user
			LOG.info("Adding " + user);
			String query = "INSERT INTO users VALUES (DEFAULT, ?)";
			PreparedStatement pst = cn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			pst.setString(1, user.getLogin());
			pst.executeUpdate();

			// Getting last insert id to update id field in this User.
			ResultSet rs = pst.getGeneratedKeys();
			if (rs.next()) user.setId(rs.getInt(1));

		} catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
		return false;
	}


	/**
	 * Returns a user by its name from a given Database.
	 * @param login
	 * @return a user from a given Database that matches a given login.
	 * @throws DBException
	 */
	public User getUser(String login) throws DBException {
		User user = null;
		try {

			String query = "SELECT * FROM users WHERE login=?";
			PreparedStatement pst = cn.prepareStatement(query);
			pst.setString(1, login);

			ResultSet rs = pst.executeQuery();
			if (rs.next()){
				int userID = rs.getInt(1);
				String userLogin = rs.getString(2);
				user = User.createUser(userLogin);
				user.setId(userID);
				LOG.info("User found " + userLogin + " id " + userID);
			}
		} catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
		return user;
	}

	/**
	 * Returns a team by its name from a given Database.
	 * @param name
	 * @return a team from a given Database that matches a given name.
	 * @throws DBException
	 */
	public Team getTeam(String name) throws DBException {
		Team team = null;
		try {
			String query = "SELECT * FROM teams WHERE name=?";
			PreparedStatement pst = cn.prepareStatement(query);
			pst.setString(1, name);

			ResultSet rs = pst.executeQuery();
			if (rs.next()){
				int teamID = rs.getInt(1);
				String teamName = rs.getString(2);
				team = Team.createTeam(teamName);
				team.setId(teamID);
				LOG.info("Team found " + teamName);
			}
		} catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
		return team;
	}

	/**
	 * Returns a list of teams in a given Database.
	 * @return a list of all teams in a given Database.
	 * @throws DBException
	 */
	public List<Team> findAllTeams() throws DBException {
		List<Team> list = null;
		try {
			Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM teams ORDER BY id");

			list = new ArrayList<>();
			while(rs.next()){
				int teamID = rs.getInt(1);
				String teamName = rs.getString(2);
				Team team = Team.createTeam(teamName);
				team.setId(teamID);
				list.add(team);
			}
			LOG.info("Teams:" + list);
		} catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
		return list;
	}

	/**
	 * Adds a new team to a given Database.
	 * @param team
	 * @return some boolean that wasn't specified in the requirements.
	 * @throws DBException
	 */
	public boolean insertTeam(Team team) throws DBException {
		try {
			// Adding team
			LOG.info("Adding " + team);
			String query = "INSERT INTO teams VALUES (DEFAULT, ?)";
			PreparedStatement pst = cn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			pst.setString(1, team.getName());
			pst.executeUpdate();

			// Getting last insert id to update id field in this Team.
			ResultSet rs = pst.getGeneratedKeys();
			if (rs.next()) team.setId(rs.getInt(1));

		} catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
		return false;
	}

	/**
	 * Adds information on user's teams to a given Database.
	 * @param user
	 * @param teams
	 * @return some boolean that wasn't specified in the requirements.
	 * @throws DBException
	 */
	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		try {
			cn.setAutoCommit(false);
			LOG.info("login=" + user.getLogin() + " id=" + user.getId() + Arrays.toString(teams));
			try {
				String query = "INSERT INTO users_teams VALUES(?, ?)";
				PreparedStatement pst = cn.prepareStatement(query);

				for(Team team: teams){
					pst.setInt(1, user.getId());
					pst.setInt(2, team.getId());
					pst.executeUpdate();
				}

				cn.commit();
			} catch (SQLException ex) {
				LOG.warning( "Rollback " + ex.getMessage());
				cn.rollback();
				throw ex;
			}
			cn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return false;	
	}

	/**
	 * Reads user's teams from users_teams table of a given Database.
	 * @param user
	 * @return list of teams set for a given user.
	 * @throws DBException
	 */
	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> list = null;
		try {
			LOG.info("User " + user.getLogin() + " id " + user.getId());
			// Why exception if users_teams.user_id?
			String query = "SELECT t.id, t.name FROM users_teams ut JOIN teams t ON ut.team_id = t.id WHERE ut.user_id=?";
			PreparedStatement pst = cn.prepareStatement(query);
			pst.setInt(1, user.getId());

			ResultSet rs = pst.executeQuery();
			list = new ArrayList<>();
			while(rs.next()){
				int teamID = rs.getInt(1);
				String teamName = rs.getString(2);
				Team team = Team.createTeam(teamName);
				team.setId(teamID);
				list.add(team);
			}
			LOG.info("User " + user.getLogin() + " teams " + list);
		} catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
		return list;
	}

	/**
	 * Deletes a team from Database.
	 * @param team
	 * @return some boolean that wasn't specified in the requirements.
	 * @throws DBException
	 */
	public boolean deleteTeam(Team team) throws DBException {
		try {
			String query = "DELETE FROM teams WHERE name=?";
			PreparedStatement pst = cn.prepareStatement(query);
			pst.setString(1, team.getName());
			pst.executeUpdate();

			LOG.info("Team " + team.getName() + " deleted");
		} catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
		return false;
	}

	/**
	 * Updates a team name in Database.
	 * @param team
	 * @return some boolean that wasn't specified in the requirements.
	 * @throws DBException
	 */
	public boolean updateTeam(Team team) throws DBException {
		try {
			String query = "UPDATE teams SET name=? WHERE id=?";
			PreparedStatement pst = cn.prepareStatement(query);
			pst.setString(1, team.getName());
			pst.setInt(2, team.getId());
			pst.executeUpdate();

			LOG.info("Team name updated to " + team.getName());
		} catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
		return false;
	}

	/**
	 * Deletes users from Database.
	 * @param users
	 * @return some boolean that wasn't specified in the requirements.
	 * @throws DBException
	 */
	public boolean deleteUsers(User... users) throws DBException {
		try {
			String query = "DELETE FROM users WHERE login=?";
			PreparedStatement pst = cn.prepareStatement(query);
			for(User user: users){
				pst.setString(1, user.getLogin());
				pst.executeUpdate();
				LOG.info("User " + user.getLogin() + " deleted");
			}
		} catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
		return false;
	}
}
